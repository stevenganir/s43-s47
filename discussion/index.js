//Create a simple JS application
//Dependancies and Modules
const express = require("express");
const mongoose = require("mongoose");
//Allows our backend application to be available to our frontend application
const cors = require("cors")

const userRoutes = require("./routes/user")
const courseRoutes = require("./routes/course")

//Environment Setup
const port = 4000;

//Server Setup

const app = express();

app.use(express.json());
app.use(express.urlencoded({extended:true}));
//Allows our backend application to be available to our frontend application
app.use(cors());

//Database Connection
	//Connect to our MongoDB Database
	mongoose.connect("mongodb+srv://steven:admin123@batch-297.mi0eika.mongodb.net/s43-s47?retryWrites=true&w=majority", 
		{
			useNewUrlParser: true,
			useUnifiedTopology: true
		}
	)

	//prompt
	let db = mongoose.connection;
	db.on("error", console.error.bind(console,"Connection error"))
	mongoose.connection.once('open', ()=>console.log('Connected to MongoDB Atlas.'))

	//[Backend Routes]
	//http://localhost:4000/users
	app.use("/users", userRoutes);
	app.use("/courses", courseRoutes)

//Server Gateway Response
if(require.main === module){
	app.listen(process.env.PORT || port, ()=>{
		console.log(`API is now online on port ${process.env.PORT || port}`)
	})
}

module.exports = {app, mongoose}