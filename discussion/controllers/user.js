//controller
const User = require("../models/User");
const Course = require("../models/Course")
const bcrypt = require("bcrypt")
const auth = require("../auth")


//Check if the email exists already
/*
	Steps:
	1."find" mongoose method to find duplicate items
	2."then" method to send a response back to the FE application based on the result of the "find" method

*/

module.exports.checkEmailExists = (reqbody)=>{

	return User.find({email:reqbody.email}).then(result=>{

		if(result.length >0){
			return true;
		}
		//no duplicate found
		//the user is not yet registered in the db
		else{
			return false;
		}

	})
}


//things added to user.js - routes
//const userController = require("../controllers/user")
//userController.checkEmailExists(req.body)//.then(resultFromController=>res.send(resultFromController))


//User Registration
/*
	Steps:
	1. create a new user object
	2. make sure that the password is encrypted
	3. save the new User to the database
*/


module.exports.registerUser = (reqbody)=>{

	let newUser = new User({
		firstName: reqbody.firstName,
		lastName: reqbody.lastName,
		email: reqbody.email,
		mobileNo: reqbody.mobileNo,
		password: bcrypt.hashSync(reqbody.password, 10)
	})

	return newUser.save().then((user,error)=>{
		if(error){
			return false
		}else{
			return true
		}
	})
	.catch(err=>err)
}

//things added to user.js -routes
//userController.registerUser(req.body)//.then(resultFromController=>res.send(resultFromController))

//User authentication

/*
	Steps:
	1. check the db if user email exists
	2. compare the password from req.body and password stored in the database
	3. Generate a JWT if the user logged in and return false if not
*/

module.exports.loginUser = (req, res)=>{

	return User.findOne({email: req.body.email}).then(result=>{
		if(result === null){
			return false
		}else{
			//compareSync method is used to compare a non-encrypted password and from the enrypted password from the db
			const isPasswordCorrect = bcrypt.compareSync(req.body.password, result.password)

			if(isPasswordCorrect){
				return res.send({access: auth.createAccessToken(result)})
			}
			//else pw does not match
			else{
				return res.send(false);
			}

		}
	})
	.catch(err=>res.send(err))
}

//things added to user.js - routes
//router.post("/login",userController.loginUser)

//Activity

//own solution
// module.exports.getProfile = (req, res) =>{

// 	return User.findOne({_id: req.body.id}).then(result=>{
// 		result.password = ""
// 		return res.send(result)
// 	})
// }

//activity sol

module.exports.getProfile = (req, res) =>{

	return User.findById(req.user.id).then(result=>{
		result.password = ""
		return res.send(result)
	})
	.catch(err => res.send(err))
}

//things added to user.js - routes
//router.post("/details", userController.getProfile)

//Retrieve all courses
/*
	1. Retrieve all the courses from the database
*/

//We will use the find() method to for our course model


module.exports.getAllCourses = (req, res) =>{

	return Course.find({}).then(result=>{
		return res.send(result)
	})
}


//Enroll user to a class
/*
	1. Find the document in the database using the user's ID
	2. Add the course ID to the user's enrollment array
	3. Update the document in the MongoDB Atlas database
*/

module.exports.enroll = async (req, res) =>{

	//to check in the terminal the user id and courseId
	console.log(req.user.id);
	console.log(req.body.courseId);

	//checks if user is an admin and deny the enrollment
	if(req.user.isAdmin){
		return res.send("Action Forbidden")
	}

	//Creates an "isUserUpdated variable and return true upon successful update otherwise returns error"
	let isUserUpdated = await User.findById(req.user.id).then(user =>{

		//Add the courseId in an object and push that object into the user's "enrollment" array
		let newEnrollment = {
			courseId: req.body.courseId
		}

		//Add the course in the enrollments array.
		user.enrollments.push(newEnrollment);

		//Save the updated user and return true if successful or the error message if failed.
		return user.save().then(user => true).catch(err => err.message);
	})

	//Checks if there are errors in updating the user
	if(isUserUpdated !== true){
		return res.send({message: isUserUpdated})
	}

	console.log(isUserUpdated)


	let isCourseUpdated = await Course.findById(req.body.courseId).then(course => {

		let enrollee = {
			userId: req.user.id
		}

		course.enrollees.push(enrollee)

		return course.save().then(course => true).catch(err=> err.message);
	})

	console.log(isCourseUpdated)

	//Checks if there are errors in updating the course
	if(isCourseUpdated !== true){
		return res.send({message: isCourseUpdated})
	}

	//Checks if both userupdate and courseupdate are successful
	if(isUserUpdated && isCourseUpdated){
		return res.send("Enrolled Successfully")
	}

}