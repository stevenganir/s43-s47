[BASH] npm init -y
[BASH] npm install express
[BASH] npm install mongoose
[BASH] touch index.js .gitignore

[.gitignore] node_modules

[index.js] //Dependencies and Modules
[index.js] const express = require("express");
[index.js] const mongoose = require("mongoose");
[index.js] //Environment Setup
[index.js] const port = 4000;
[index.js] //Server Setup
[index.js] const app = express();
[index.js] app.use(express.json());
[index.js] app.use(express.urlencoded({extended:true}));
[index.js] //Database Connection
[index.js] 		//Connecting to MongoDB Database
[index.js]	
	mongoose.connect("mongodb+srv://steven:admin123@batch-297.mi0eika.mongodb.net/s43-s47?retryWrites=true&w=majority", 
		{
			useNewUrlParser: true,
			useUnifiedTopology: true
		}
	)
[index.js]		//bash prompt
[index.js] 
	let db = mongoose.connection;
	db.on("error", console.error.bind(console,"Connection error"))
	mongoose.connection.once('open', ()=>console.log('Connected to MongoDB Atlas.'))
[index.js] //Server Gateway Response
[index.js]
	if(require.main === module){
		app.listen(process.env.PORT || port, ()=>{
			console.log(`API is now online on port ${process.env.PORT || port}`)
		})
	}
[index.js] //Exporting Express and Mongoose
[index.js]
	module.exports = {app, mongoose}

[BASH] nodemon index.js

[File explorer] Create folder of models

[Sublime] Create *model*.js

[*model*.js] const mongoose = require("mongoose");
[*model*.js] //Schema/Blueprint
[*model*.js] 
	const *schema_name*Schema = new mongoose.Schema({
		firstName: {
		type: String,
		required: [true, "First name is required"]
		}
	)}
[*model*.js] //Exporting model
[*model*.js] module.exports = mongoose.model("*Schema_name*", *schema_name*Schema);

[File explorer] Create controllers folder

[Sublime] Create *controller*.js

[*controller*.js] const User = require("../models/User");
[*controller*.js] create business logic in comment (title and steps)
[*controller*.js]
	~SAMPLE~
	module.exports.*route* = (req, res)=>{
		return User.findById(req.user.id).then(result=>{
			result.password = ""
			return res.send(result)
		})
		.catch(err => res.send(err))
	}

[File explorer] Create routes folder

[Sublime] Create *route*.js

[*route*.js] //Dependencies and Modules
[*route*.js] const express = require("express");
[*route*.js] const *controller*Controller = require("../controllers/*controller")
[*route*.js] //Routing component
[*route*.js] const router = express.Router()
[*route*.js] //ROUTES
[*route*.js] router.*method*("/*header*", *controller*Controller.*route*)
[*route*.js] //Export Route System
[*route*.js] module.exports = router;

[index.js] @ after //Dependencies and Modules
[index.js] const *route*Routes = require (./routes/*route*)
[index.js] @ after //Database Connection
[index.js] app.use("/*header*", *route*Routes);